import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { DataService } from '../../services/data.service';
import { User } from '../../models/User';
import { MatTableDataSource } from '@angular/material/table';
import { UserEditComponent } from '../user-edit/user-edit.component';
import { UserDeleteComponent } from '../user-delete/user-delete.component';
import { EmployeeCreateComponent } from '../employee-create/employee-create.component';
import { select, Store } from '@ngrx/store';
import { Observable, Subscription } from 'rxjs';
import { getCount, getUsers } from '../../store/user.selector';
import { loadUsers } from '../../store/user.actions';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {
  // variable intialization starts
  users: Observable<User[]>;
  count: Observable<number>;
  private subscription: Subscription;
  displayedColumns = ['id', 'name', 'user_name', 'email', 'phone', 'company', 'city', 'actions'];
  dataSource: MatTableDataSource<User>;
  tempData: any;
  // user$: any = {};
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  // variable intialization ends

  constructor(public dialogService: MatDialog,
    public dataService: DataService, private store: Store<any>) { }


  ngOnInit() {
    this.users = this.store.select(getUsers);
    this.count = this.store.select(getCount);
    this.store.dispatch(loadUsers());
    this.loadData();
  }

  // edit user dialog
  editUser(i: number) {
    this.dialogService.open(UserEditComponent, {
      data: { data: this.tempData, index: i },
    }).afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }
  // edit user dialog

  // delete user dialog
  deleteUser(i: number) {
    this.dialogService.open(
      UserDeleteComponent, {
      data: { data: this.tempData, index: i },
    }).afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }
  // delete user dialog

  // create user dialog
  createEmployee() {
    const dialogRef = this.dialogService.open(
      EmployeeCreateComponent);
    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }
  // create user dialog

  // get user data
  loadData() {
    this.subscription = this.users.subscribe(data => {
      this.tempData = data;
      this.dataSource = new MatTableDataSource(data);
      this.dataSource.sort = this.sort;
    })
  }
  // get user data

  ngOnDestroy(){
    this.subscription.unsubscribe();
  }



}
