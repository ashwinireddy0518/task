import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Store } from '@ngrx/store';
import { DataService } from '../../services/data.service';
import { deleteUser } from '../../store/user.actions';

@Component({
  selector: 'app-user-delete',
  templateUrl: './user-delete.component.html',
  styleUrls: ['./user-delete.component.css']
})
export class UserDeleteComponent implements OnInit {
  // variable intialization starts
  userData: any;
  // variable intialization ends

  constructor(public dialogRef: MatDialogRef<UserDeleteComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any, public dataService: DataService, private store: Store<any>) { }


  ngOnInit() {
    this.userData = this.data.data[this.data.index];
  }

  // close dialog
  onNoClick(): void {
    this.dialogRef.close();
  }
  // close dialog

  // delete User
  confirmDelete(): void {
    this.store.dispatch(deleteUser({ id: this.userData.id }));
  }
  // delete User



}
