import { usersAdapter, UserState } from './user.state';
import { createFeatureSelector, createSelector } from '@ngrx/store';

export const USER_STATE_NAME = 'users';
const getUsersState = createFeatureSelector<UserState>(USER_STATE_NAME);
export const usersSelectors = usersAdapter.getSelectors();

export const getUsers = createSelector(getUsersState, usersSelectors.selectAll);
export const getUserEntities = createSelector(
    getUsersState,
    usersSelectors.selectEntities
);


export const getCount = createSelector(getUsersState, (state) => state.count);