import { getUsers } from './user.selector';
import { Store } from '@ngrx/store';
import { Update } from '@ngrx/entity';
import { User } from '../models/User';
import {
  map,
  mergeMap,
  withLatestFrom,
} from 'rxjs/operators';
import {
  deleteUser,
  deleteUserSuccess,
  loadUsers,
  loadUsersSuccess,
  updateUser,
  updateUserSuccess,
} from './user.actions';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Injectable } from '@angular/core';
import { DataService } from '../services/data.service';

@Injectable()
export class UsersEffects {
  constructor(
    private actions$: Actions,
    private dataService: DataService,
    private store: Store<any>
  ) {}

  loadUsers$ = createEffect(
    () => {
    return this.actions$.pipe(
      ofType(loadUsers),
      withLatestFrom(this.store.select(getUsers)),
      mergeMap((action) => {
          return this.dataService.getAllUsers().pipe(
            map((posts) => {
              return loadUsersSuccess({ posts });
            })
          );
      })
    );
  });


  updateUser$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(updateUser),
      map((action) => {
            const updatedUser: Update<User> = {
              id: action.post.id,
              changes: {
                ...action.post,
              },
            };
            return updateUserSuccess({ post: updatedUser });
      })
    );
  });

  deleteUser$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(deleteUser),
      map((action) => {
            return deleteUserSuccess({ id: action.id });
      })
    );
  });

}