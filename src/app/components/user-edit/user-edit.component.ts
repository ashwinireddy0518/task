import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Component, Inject, OnInit } from '@angular/core';
import { DataService } from '../../services/data.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';
import { updateUser } from '../../store/user.actions';


@Component({
  selector: 'app-user-edit',
  templateUrl: './user-edit.component.html',
  styleUrls: ['./user-edit.component.css']
})
export class UserEditComponent implements OnInit {
  // variable intialization starts
  userForm: FormGroup;
  userData: any;
  // variable intialization ends

  constructor(public dialogRef: MatDialogRef<UserEditComponent>, private fb: FormBuilder,
    @Inject(MAT_DIALOG_DATA) public data: any, public dataService: DataService, private store: Store<any>) { }


  ngOnInit() {
    this.userData = this.data.data[this.data.index];
    this.formIntialization();
    this.updateValues();

  }

  // form intialization method starts
  formIntialization() {
    this.userForm = this.fb.group({
      id: ['', Validators.required],
      name: ['', Validators.required],
      username: ['', Validators.required],
      email: ['', Validators.required],
      phone: ['', Validators.required],
      website: ['', Validators.required],
      address: this.fb.group({
        street: ['', Validators.required],
        suite: ['', Validators.required],
        city: ['', Validators.required],
        zipcode: ['', Validators.required],
        geo: this.fb.group({
          lat: ['', Validators.required],
          lng: ['', Validators.required],
        })
      }),
      company: this.fb.group(
        {
          name: ['', Validators.required],
          catchPhrase: ['', Validators.required],
          bs: ['', Validators.required],
        }
      ),
    });
  }
  // form intialization method ends

  // patch form value
  updateValues() {
    this.userForm.patchValue({
      id: this.userData.id,
      name: this.userData.name,
      username: this.userData.username,
      email: this.userData.email,
      phone: this.userData.phone,
      website: this.userData.website,
      address: {
        street: this.userData.address.street,
        suite: this.userData.address.suite,
        city: this.userData.address.city,
        zipcode: this.userData.address.zipcode,
        geo: {
          lat: this.userData.address.geo.lat,
          lng: this.userData.address.geo.lng,
        }
      },

      company: {
        name: this.userData.company.name,
        catchPhrase: this.userData.company.catchPhrase,
        bs: this.userData.company.bs
      }
    });

  }
  // patch form value

  // submit form
  onSubmit() {
    this.data.data[this.data.index] = this.userForm.value;
    this.store.dispatch(updateUser({post:this.data.data[this.data.index]}));
    this.dialogRef.close();
  }
  // submit form

  // close dialog
  onNoClick(): void {
    this.dialogRef.close();
  }
  // close dialog


}