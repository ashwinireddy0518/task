import {  HttpClientModule } from '@angular/common/http';
import { TestBed } from '@angular/core/testing';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { DataService } from './data.service';

describe('DataService', () => {
  let service: DataService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientModule, NoopAnimationsModule
      ],
      providers:[DataService]
  
    });

    service = TestBed.inject(DataService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });


 
  it('getAllUsers() should call http Get method for the given route', () => {

    service.getAllUsers();
    });


    

});
