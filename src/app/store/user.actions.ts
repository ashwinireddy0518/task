import { Update } from '@ngrx/entity';
import {User } from '../models/User';
import {createAction, props } from '@ngrx/store';


export const UPDATE_USER_ACTION = '[posts page] update post';
export const UPDATE_USER_SUCCESS = '[posts page] update post success';
export const DELETE_USER_ACTION = '[posts page] delete post';
export const DELETE_USER_SUCCESS = '[posts page] delete post success';
export const LOAD_USERS = '[posts page] load posts';
export const LOAD_USERS_SUCCESS = '[posts page] load posts success';

export const updateUser = createAction(
  UPDATE_USER_ACTION,
  props<{ post: User }>()
);

export const updateUserSuccess = createAction(
  UPDATE_USER_SUCCESS,
  props<{ post: Update<User> }>()
);

export const deleteUser = createAction(
  DELETE_USER_ACTION,
  props<{ id: string }>()
);
export const deleteUserSuccess = createAction(
  DELETE_USER_SUCCESS,
  props<{ id: string }>()
);

export const loadUsers = createAction(LOAD_USERS);
export const loadUsersSuccess = createAction(
  LOAD_USERS_SUCCESS,
  props<{ posts:User[] }>()
);

