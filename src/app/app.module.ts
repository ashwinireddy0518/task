import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {HttpClientModule} from '@angular/common/http';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { UserListComponent } from './components/user-list/user-list.component';
import { UserEditComponent } from './components/user-edit/user-edit.component';
import {MaterialModule} from './material.module';
import {MatNativeDateModule} from '@angular/material/core';
import { UserDeleteComponent } from './components/user-delete/user-delete.component';
import { EmployeeCreateComponent } from './components/employee-create/employee-create.component';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { StoreModule } from '@ngrx/store';
import { usersReducer } from './store/user.reducer';
import { EffectsModule } from '@ngrx/effects';
import { UsersEffects } from './store/user.effect';

@NgModule({
  declarations: [
    AppComponent,
    UserListComponent,
    UserEditComponent,
    UserDeleteComponent,
    EmployeeCreateComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    FormsModule,
    MatNativeDateModule,
    ReactiveFormsModule,
    MaterialModule,
    StoreModule.forRoot({
      users: usersReducer
    }),
    EffectsModule.forRoot([UsersEffects])
  ],
  providers: [ {provide: MatDialogRef, useValue: {}},
    {provide: MAT_DIALOG_DATA, useValue: []},],
  bootstrap: [AppComponent],
  entryComponents: [UserEditComponent,UserDeleteComponent,EmployeeCreateComponent],
})
export class AppModule { }
