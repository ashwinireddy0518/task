import { EntityState, createEntityAdapter } from '@ngrx/entity';
import {User } from '../models/User';

export interface UserState extends EntityState<User> {
  count: number;
}

export const usersAdapter = createEntityAdapter<User>();

export const initialState: UserState = usersAdapter.getInitialState({
  count: 0,
});
