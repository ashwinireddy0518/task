import {
    loadUsersSuccess,
    updateUserSuccess,
    deleteUserSuccess,
  } from './user.actions';
  import { Action, createReducer, on } from '@ngrx/store';
  import { initialState, usersAdapter, UserState } from './user.state';
  
  const _postsReducer = createReducer(
    initialState,
    on(updateUserSuccess, (state, action) => {
      return usersAdapter.updateOne(action.post, state);
    }),
    on(deleteUserSuccess, (state, { id }) => {
      return usersAdapter.removeOne(id, state);
    }),
    on(loadUsersSuccess, (state, action) => {
      return usersAdapter.setAll(action.posts, {
        ...state,
        count: state.count + 1,
      });
    })
  );
  
  export function usersReducer(state: UserState | undefined, action: Action) {
    return _postsReducer(state, action);
  }