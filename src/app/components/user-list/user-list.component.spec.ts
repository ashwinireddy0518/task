
import { HttpClientModule } from '@angular/common/http';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatDialog } from '@angular/material/dialog';
import { MatSort } from '@angular/material/sort';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { Store, StoreModule } from '@ngrx/store';
import { of } from 'rxjs';
import { MaterialModule } from '../../material.module';
import { DataService } from '../../services/data.service';
import {  MockStore, provideMockStore } from '@ngrx/store/testing';
import { UserListComponent } from './user-list.component';




export class MatDialogMock {
  open() {
    return {
      afterClosed: () => of({ action: true })
    };
  }
}

describe('UserListComponent', () => {
  let component: UserListComponent;
  let fixture: ComponentFixture<UserListComponent>;
  let dataService: DataService;
  let store: MockStore;
  const initialState = { };
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        MaterialModule, HttpClientModule, NoopAnimationsModule
      ],
      declarations: [UserListComponent],
      providers: [{ provide: MatDialog, useClass: MatDialogMock }, DataService, provideMockStore({ initialState })]
    })
      .compileComponents();
      store = TestBed.inject(MockStore);
    fixture = TestBed.createComponent(UserListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  beforeEach(async () => {
    fixture = TestBed.createComponent(UserListComponent);
    component = fixture.componentInstance;
    dataService = TestBed.inject(DataService);
    component.ngOnInit();
    await fixture.whenStable();
    fixture.detectChanges();
  });


  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should open the dialog', () => {
    component.editUser(1);
    component.deleteUser(1);
    component.createEmployee();
  });

  it('should call loadData', () => {
    component.loadData();
  });


  // it('initially sets up sorting', () => {
  //   fixture.detectChanges();
  //   const sort = component.dataSource.sort;
  //   expect(sort).toBeInstanceOf(MatSort);
  // });


});


