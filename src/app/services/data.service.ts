import { Injectable } from '@angular/core';
import { BehaviorSubject, catchError, Observable, Subject } from 'rxjs';
import { map, tap } from "rxjs/operators";
import { User } from '../models/User';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class DataService {
  // variable intialization starts
  private API_URL = 'https://jsonplaceholder.typicode.com/users';
  // variable intialization ends

  constructor(private httpClient: HttpClient) {
  }

  //  get all user api call
  getAllUsers() {
    return this.httpClient.get<User[]>(this.API_URL).pipe(
      map((data) => {
        const posts: User[] = [];      
        for (let key in data) {
          posts.push({ ...data[key], id: parseInt(key) });
        }
        return posts;
      }),
     
    );
  }
  //  get all user api call

  // error handler
  private handleError<T>(operation = 'operation') {
    return (error: HttpErrorResponse): Observable<T> => {
      console.error(error);
      const message = ` server returned code ${error.status} with body "${error.error}"`;
      throw new Error(`${operation} failed: ${message}`)
    };
  }
  // error handler



}
