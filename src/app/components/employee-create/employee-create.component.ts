import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-employee-create',
  templateUrl: './employee-create.component.html',
  styleUrls: ['./employee-create.component.css']
})
export class EmployeeCreateComponent implements OnInit {
  // variable intialization starts
  empForm: FormGroup;
  roles = ['Admin', 'Devloper'];
  activeValues = ['YES', 'NO'];
  submitted = false;
  // variable intialization end

  constructor(public dialogRef: MatDialogRef<EmployeeCreateComponent>, private fb: FormBuilder) { }

  ngOnInit(): void {
    this.formInit();
  }
  // form intialization method starts
  formInit() {
    this.empForm = this.fb.group({
      username: ['', Validators.required],
      location: ['', Validators.required],
      role: ['', Validators.required],
      active: ['', Validators.required]
    })
  }
  // form intialization method ends

  // submit form 
  onSubmit() {
    this.submitted = true;
    if(this.empForm.valid){
      console.log("ko",this.empForm.value)
    }
    
    this.dialogRef.close();
  }
  // submit form 

  // close dialog
  onNoClick(): void {
    this.dialogRef.close();
  }
  // close dialog

}
